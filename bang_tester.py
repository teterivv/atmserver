from bang_database import BangDatabase
import mysql.connector
import unittest
import requests


def init_database(test):
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        password=""
    )
    cursor = db.cursor(buffered=True)

    try:
        cursor.execute('CREATE DATABASE testbank')
    except:
        pass

    db.commit()

    db = mysql.connector.connect(
        host="localhost",
        user="root",
        password="",
        database="testbank"
    )
    cursor = db.cursor(buffered=True)

    sql = "DROP TABLE IF EXISTS customers, services, transactions"
    cursor.execute(sql)

    sql = "CREATE TABLE customers (" \
          "id INT AUTO_INCREMENT PRIMARY KEY, " \
          "name VARCHAR(255), " \
          "card_number VARCHAR(255)," \
          "pin INT, " \
          "balance INT," \
          "email VARCHAR(255)," \
          "password VARCHAR(255))"
    cursor.execute(sql)

    sql = "CREATE TABLE services (" \
          "id INT AUTO_INCREMENT PRIMARY KEY, " \
          "name VARCHAR(255), " \
          "type VARCHAR(255))"
    cursor.execute(sql)

    sql = "CREATE TABLE transactions (" \
          "id INT AUTO_INCREMENT PRIMARY KEY, " \
          "card_number VARCHAR(255), " \
          "message VARCHAR(255))"
    cursor.execute(sql)

    db.commit()

    sql = "INSERT INTO customers (name, card_number, pin, balance, email, password) VALUES (%s, %s, %s, %s, %s, %s)"
    val = ("Misha Hordin", "1234123412341234", "1234", "1000", "netnikare@gmail.com", "pudgepos5")
    cursor.execute(sql, val)

    val = ("Dima Larin", "1111222233334444", "1111", "2", "dima_larin@dota.com", "voidpos1")
    cursor.execute(sql, val)

    db.commit()

    sql = "INSERT INTO services (name, type) VALUES (%s, %s)"

    val = ("КП Водоканал", "water")
    cursor.execute(sql, val)
    val = ("КП Водопостачання", "water")
    cursor.execute(sql, val)
    val = ("КП Київський Водоканал", "water")
    cursor.execute(sql, val)

    val = ("КП УкрГазЗбут", "gas")
    cursor.execute(sql, val)
    val = ("КП Газопостачання", "gas")
    cursor.execute(sql, val)
    val = ("КП КиївГАЗ", "gas")
    cursor.execute(sql, val)

    val = ("КП Укрсвітло", "electricity")
    cursor.execute(sql, val)
    val = ("КП 'OpenEnergy'", "electricity")
    cursor.execute(sql, val)
    val = ("КП ЖОЕК", "electricity")
    cursor.execute(sql, val)

    val = ("ОСББ #1", "house")
    cursor.execute(sql, val)
    val = ("ОСББ #2", "house")
    cursor.execute(sql, val)
    val = ("ОСББ #3", "house")
    cursor.execute(sql, val)

    val = ("КП Українська дистанція звязку", "connection")
    cursor.execute(sql, val)
    val = ("ООО Київстар, дом. інтернет", "connection")
    cursor.execute(sql, val)
    val = ("ООО Оріон, дом. інтернет", "connection")
    cursor.execute(sql, val)

    val = ("КВГП Вивіз Сміття", "trash")
    cursor.execute(sql, val)
    val = ("КП Сміттєвози)", "trash")
    cursor.execute(sql, val)

    db.commit()

    return test


class BangTester:
    def __init__(self):
        db = mysql.connector.connect(
            host="localhost",
            user="root",
            password=""
        )
        cursor = db.cursor(buffered=True)

        try:
            cursor.execute('CREATE DATABASE testbank')
        except:
            pass

        db.commit()

    def run_all_tests(self):
        self.test_login_by_card()
        self.test_login_by_email()
        self.test_change_pin()
        self.test_balance()
        self.test_services()
        self.test_service()
        self.test_top_in()
        self.test_top_up()
        self.test_mobile()
        self.test_transactions()

    @init_database
    def test_login_by_card(self):
        url = 'localhost:5000/bangbank/login'
        obj = {
            'type' : 'card',
            'card' : '1234123412341234',
            'pin'  : '1234'
        }

        response = requests.post(url, data=obj)
        print(response.json())
        print(response.text)
        print(response)


    @init_database
    def test_login_by_email(self):
        pass

    @init_database
    def test_change_pin(self):
        pass

    @init_database
    def test_balance(self):
        pass

    @init_database
    def test_services(self):
        pass

    @init_database
    def test_service(self):
        pass

    @init_database
    def test_top_in(self):
        pass

    @init_database
    def test_top_up(self):
        pass

    @init_database
    def test_mobile(self):
        pass

    @init_database
    def test_transactions(self):
        pass

if __name__ == "__main__":
    tester = BangTester()
    tester.run_all_tests()