from flask import Flask, session, request, jsonify
from bang_database import BangDatabase
from flask_classful import FlaskView, route

from bang_tester import BangTester

app = Flask(__name__)
app.secret_key = b'MishaHordinPudgePos5'
app.config["DEBUG"] = True


class BangServer(FlaskView):
    route_base = '/'

    def __init__(self, database="bangbank"):
        self.db = BangDatabase(database)
        self.current_language = "en"

    @route("/", methods=['GET', 'POST'])
    def main(self):
        print("BANG BANG")

    @route("/bangbank/transactions", methods=['GET', 'POST'])
    def transactions(self):
        return {
            'status': 200,
            'transactions': self.db.transactions(session['card_number'])
        }

    @route("/bangbank/service", methods=['POST'])
    def service(self):
        if 'id' not in request.form or 'value' not in request.form or 'account' not in request.form:
            return {
                'status': 400,
                'message': "Уведіть номер рахунку та/або суму оплати"
                if self.current_language == "ua" else "Provide account number and/or payment value"
            }
        else:
            id = request.form['id']
            value = int(request.form['value'])
            balance = int(db.balance(session['card_number']))
            account = request.form['account']

            if balance >= value:
                if 'counter_start' in request.form and 'counter_end' in request.form:
                    counter_start = request.form['counter_start']
                    counter_end = request.form['counter_end']
                    self.db.pay_service(session['card_number'], id, value, account, counter_start, counter_end)
                else:
                    self.db.pay_service(session['card_number'], id, value, account)
                return {
                    'status': 200,
                    'message': "Ви успішно сплатили послугу"
                    if self.current_language == "ua" else "Service payment went successfully"
                }
            else:
                return {
                    'status': 400,
                    'message': "У вас на рахунку недостатньо коштів!"
                    if self.current_language == "ua" else "You have not enough money on the balance."
                }

    @route("/bangbank/services", methods=['POST'])
    def services(self):
        if 'type' not in request.form:
            return {
                'status': 400,
                'message': "Уведіть вид комунальної послуги"
                if self.current_language == "ua" else "Provide type of service"
            }
        else:
            type = request.form['type']
            services = self.db.services(type)
            return {
                'status': 200,
                'services': services
            }

    @route("/bangbank/pin", methods=['POST'])
    def pin(self):
        if 'pin' not in request.form:
            return {
                'status': 400,
                'message': "Уведіть новий пін"
                if self.current_language == "ua" else "Provide new pin"
            }
        else:
            pin = request.form['pin']

            self.db.update_pin(session['card_number'], pin)
            return {
                'status': 200,
                'message': "Пін код успішно змінено."
                if self.current_language == "ua" else "Pin code changed successfully"
            }

    @route("/bangbank/top_up", methods=['POST'])
    def top_up(self):
        if 'value' not in request.form:
            return {
                'status': 400,
                'message': "Введіть сумму для поповнення"
                if self.current_language == "ua" else "Provide value for top up"
            }
        else:
            value = int(request.form['value'])
            if value >= 0:
                self.db.top_up(session['card_number'], value)
                return {
                    'status': 200,
                    'value': value,
                    'message': "Рахунок успішно поповнено"
                    if self.current_language == "ua" else "Account successfully topped up"
                }
            else:
                return {
                    'status': 400,
                    'value': value,
                    'message': "Число має бути додатнім."
                    if self.current_language == "ua" else "Must be positive number."
                }

    @route("/bangbank/phone", methods=['POST'])
    def phone(self):
        if 'phone_number' not in request.form or 'value' not in request.form:
            return {
                'status': 400,
                'message': "Заповніть усі поля"
                if self.current_language == "ua" else "Provide number and value"
            }
        else:
            balance = int(db.balance(session['card_number']))
            value = int(request.form['value'])

            if balance >= value:
                self.db.top_phone(session['card_number'], value)
                return {
                    'status': 200,
                    'message': "Телефон успішно поповнено!"
                    if self.current_language == "ua" else "Phone was charged successfully!"
                }
            else:
                return {
                    'status': 400,
                    'message': "Недостатньо коштів на рахунку."
                    if self.current_language == "ua" else "Not enough money on the balance"
                }

    @route("/bangbank/balance", methods=['GET'])
    def balance(self):
        return {
            'status': 200,
            'balance': self.db.balance(session['card_number'])
        }

    @route("/bangbank/withdraw", methods=['POST'])
    def withdraw(self):
        if request.method == 'POST':
            if 'value' in request.form:
                current_balance = self.db.balance(session['card_number'])
                value = request.form['value']
                if int(current_balance) >= int(value):
                    self.db.withdraw(session['card_number'], value)
                    return {
                        'status': 200,
                        'value': value,
                        'message': "Кошти успішно зняті"
                        if self.current_language == "ua" else "Withdrawal went successfully"
                    }
                else:
                    return {
                        'status': 400,
                        'value': value,
                        'message': "Недостатньо коштів на рахунку"
                        if self.current_language == "ua" else "Not enough money"
                    }
            else:
                return {
                    "status": 400,
                    'message': 'Provide a value to withdraw'
                }

    @route('/bangbank/login', methods=['POST'])
    def login(self):
        if 'type' in request.form and request.form['type'] == 'card':
            if 'card_number' not in request.form or 'pin' not in request.form:
                return {
                    "status": 400,
                    "message": "card_number and/or pin_code parameters not provided"
                }

            card_number = request.form['card_number']
            pin_code = request.form['pin']
            result = self.db.verify_by_card_number(card_number, pin_code)
            if result:
                session['card_number'] = card_number
                return {
                    "status": 200,
                    "balance": self.db.balance(card_number),
                    "name": self.db.name(card_number)
                }
            else:
                return {
                    "status": 404,
                    "message": "Картка не знайдена або пін код невірний"
                    if self.current_language == "ua" else "Card number or pin incorrect"
                }
        elif 'type' in request.form and request.form['type'] == 'email':
            username = request.form['email']
            password = request.form['password']

            result = self.db.verify_by_username(username, password)

            if result:
                card_number = self.db.card_number(username)
                session['card_number'] = card_number
                return {
                    "status": 200,
                    "balance": self.db.balance(card_number),
                    "name": self.db.name(card_number)
                }
            else:
                return {
                    "status": 400,
                    "message": "username and/or password parameters not provided"
                }
        else:
            return {
                "status": 400,
                "message": "Provide type of auth"
            }


BangServer.register(app, route_base='/', trailing_slash=False, init_argument='testbank')

if __name__ == '__main__':
    is_testing = True

    if is_testing:
        pass
        # BangServer.register(app, 'testbank')
    else:
        BangServer.register(app)

    app.run()