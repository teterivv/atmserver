import mysql.connector

class BangDatabase:
    def __init__(self, database="bangbank", password=""):
        self._db = mysql.connector.connect(
                host="localhost",
                user="root",
                password=password,
                database=database)
        self._cursor = self._db.cursor(buffered=True)

    def _tuple_to_dict(self, row):
        return {
            "id" : str(row[0]),
            "name" : str(row[1]),
            "card_number": str(row[2]),
            "pin" : str(row[3]),
            "balance" : str(row[4]),
            "email" : str(row[5]),
            "password" : str(row[6])
        }

    def _get_user_by_card_number(self, card_number):
        sql = "SELECT * FROM customers WHERE card_number = %s"
        val = (card_number, )
        self._cursor.execute(sql, val)

        result = self._cursor.fetchone()
        if result is None:
            raise LookupError("Customer not found")

        return self._tuple_to_dict(result)

    def _get_user_by_username(self, username):
        sql = "SELECT * FROM customers WHERE email = %s"
        val = (username, )
        self._cursor.execute(sql, val)

        result = self._cursor.fetchone()
        if result is None:
            raise LookupError("Customer not found")

        return self._tuple_to_dict(result)

    def _change_balance(self, card_number, value):
        result = self._get_user_by_card_number(card_number)
        sql = "UPDATE customers SET balance = %s WHERE card_number = %s"
        val = (int(result['balance']) - int(value), card_number)
        self._cursor.execute(sql, val)

        self._db.commit()

    def _insert_transaction(self, card_number, message):
        sql = "INSERT INTO transactions (card_number, message) VALUES (%s, %s)"
        val = (card_number, message)
        self._cursor.execute(sql, val)

        self._db.commit()

    def _service_name(self, id):
        sql = "SELECT * FROM services WHERE id = %s"
        val = (id, )
        self._cursor.execute(sql, val)

        result = self._cursor.fetchone()
        return result[1]

    def verify_by_card_number(self, card_number, pin):
        try:
            if self.pin(card_number).strip() == pin:
                return True
            return False
        except LookupError:
            return False

    def balance(self, card_number):
        result = self._get_user_by_card_number(card_number)
        return result['balance']

    def name(self, card_number):
        result = self._get_user_by_card_number(card_number)
        return result['name']

    def pin(self, card_number):
        result = self._get_user_by_card_number(card_number)
        return result['pin']

    def verify_by_username(self, username, password):
        try:
            result = self._get_user_by_username(username)

            if result["password"].strip() == password:
                return True
            return False
        except LookupError:
            return False

    def card_number(self, username):
        result = self._get_user_by_username(username)
        return result['card_number']

    def withdraw(self, card_number, value):
        self._change_balance(card_number, value)
        self._insert_transaction(card_number, "Знято готівки: {0} у.о.".format(value))

    def update_pin(self, card_number, pin):
        sql = "UPDATE customers SET pin = %s WHERE card_number = %s"
        val = (pin, card_number)
        self._cursor.execute(sql, val)

        self._db.commit()

    def services(self, type):
        sql = "SELECT * FROM services WHERE type = %s"
        val = (type, )
        self._cursor.execute(sql, val)

        results = self._cursor.fetchall()
        services = []
        for result in results:
            services.append({
                'id' : result[0],
                'name' : result[1]
            })
        return services

    def pay_service(self, card_number, id, value, account, counter_start = None, counter_end = None):
        message = "Платіж: {0}, " \
                  "Рахунок: {1}, " \
                  "Сума: {2}, ".format(self._service_name(id), account, value)
        if counter_start is not None:
            message += "Поч. лічильник: {0}," \
                       "Кін. лічильник: {1}".format(counter_start, counter_end)
        self._insert_transaction(card_number, message)
        self._change_balance(card_number, value)

    def top_up(self, card_number, value):
        self._change_balance(card_number, -value)
        self._insert_transaction(card_number, "Рахунок поповнено на {0} y.o.".format(value))

    def top_phone(self, card_number, value):
        self._change_balance(card_number, value)
        self._insert_transaction(card_number, "Телефон поповнено на {0} y.o.".format(value))

    def transactions(self, card_number):
        sql = 'SELECT * FROM transactions WHERE card_number = %s'
        val = (card_number, )
        self._cursor.execute(sql, val)
        results = self._cursor.fetchall()

        transactions = []
        for result in results:
            transactions.append(result[2])
        return transactions
