import mysql.connector

if __name__ == "__main__":
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        password="13121312"
    )
    cursor = db.cursor(buffered=True)
    cursor.execute("SHOW DATABASES")

    try:
        print("Trying to create BangBank database.")
        cursor.execute('CREATE DATABASE bangbank')
        print("BangBank database created.")
    except:
        print("BangBank database exists, proceeding..")

    db = mysql.connector.connect(
        host="localhost",
        user="root",
        password="13121312",
        database="bangbank"
    )
    cursor = db.cursor(buffered=True)

    sql = "DROP TABLE IF EXISTS customers, services, transactions"
    print(sql)
    cursor.execute(sql)

    sql = "CREATE TABLE customers (" \
          "id INT AUTO_INCREMENT PRIMARY KEY, " \
          "name VARCHAR(255), " \
          "card_number VARCHAR(255)," \
          "pin INT, " \
          "balance INT," \
          "email VARCHAR(255)," \
          "password VARCHAR(255))"
    print(sql)
    cursor.execute(sql)

    sql = "CREATE TABLE services (" \
          "id INT AUTO_INCREMENT PRIMARY KEY, " \
          "name VARCHAR(255), " \
          "type VARCHAR(255))"
    print(sql)
    cursor.execute(sql)

    sql = "CREATE TABLE transactions (" \
          "id INT AUTO_INCREMENT PRIMARY KEY, " \
          "card_number VARCHAR(255), " \
          "message VARCHAR(255))"
    print(sql)
    cursor.execute(sql)

    db.commit()

    sql = "INSERT INTO customers (name, card_number, pin, balance, email, password) VALUES (%s, %s, %s, %s, %s, %s)"
    val = ("Misha Hordin", "1234123412341234", "1234", "1000", "netnikare@gmail.com", "pudgepos5")
    print(sql, val)
    cursor.execute(sql, val)

    val = ("Dima Larin", "1111222233334444", "1111", "2", "dima_larin@dota.com", "voidpos1")
    print(sql, val)
    cursor.execute(sql, val)

    db.commit()

    sql = "INSERT INTO services (name, type) VALUES (%s, %s)"
    print(sql)

    val = ("КП Водоканал", "water")
    cursor.execute(sql, val)
    val = ("КП Водопостачання", "water")
    cursor.execute(sql, val)
    val = ("КП Київський Водоканал", "water")
    cursor.execute(sql, val)

    val = ("КП УкрГазЗбут", "gas")
    cursor.execute(sql, val)
    val = ("КП Газопостачання", "gas")
    cursor.execute(sql, val)
    val = ("КП КиївГАЗ", "gas")
    cursor.execute(sql, val)

    val = ("КП Укрсвітло", "electricity")
    cursor.execute(sql, val)
    val = ("КП 'OpenEnergy'", "electricity")
    cursor.execute(sql, val)
    val = ("КП ЖОЕК", "electricity")
    cursor.execute(sql, val)

    val = ("ОСББ #1", "house")
    cursor.execute(sql, val)
    val = ("ОСББ #2", "house")
    cursor.execute(sql, val)
    val = ("ОСББ #3", "house")
    cursor.execute(sql, val)

    val = ("КП Українська дистанція звязку", "connection")
    cursor.execute(sql, val)
    val = ("ООО Київстар, дом. інтернет", "connection")
    cursor.execute(sql, val)
    val = ("ООО Оріон, дом. інтернет", "connection")
    cursor.execute(sql, val)

    val = ("КВГП Вивіз Сміття", "trash")
    cursor.execute(sql, val)
    val = ("КП Сміттєвози)", "trash")
    cursor.execute(sql, val)

    db.commit()




